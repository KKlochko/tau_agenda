defmodule TauAgenda.Generators do
  @moduledoc "Generators for tests"

  use Ash.Generator

  @doc """
  Generates user changesets with the `:register_with_password` action.
  """
  def user(opts \\ []) do
    changeset_generator(
      TauAgenda.Accounts.User,
      :register_with_password,
      defaults: [
        # Generates unique values using an auto-incrementing sequence
        # eg. `user1@example.com`, `user2@example.com`, etc.
        email: sequence(:user_email, &"user#{&1}@example.com"),
        password: "password",
        password_confirmation: "password"
      ],
      overrides: opts,
      authorize?: false
    )
  end

  def task(opts \\ []) do
    actor =
      opts[:actor] ||
        once(:default_actor, fn ->
          generate(user())
        end)

    changeset_generator(
      TauAgenda.Tasks.Task,
      :create,
      defaults: [
        name: sequence(:name, &"Task #{&1}"),
        description: sequence(:name, &"Description #{&1}")
      ],
      overrides: opts,
      actor: actor
    )
  end
end
