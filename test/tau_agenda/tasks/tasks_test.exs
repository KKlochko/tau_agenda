defmodule TauAgenda.TasksTest do
  use TauAgenda.DataCase, async: true

  alias TauAgenda.Tasks

  describe "calculate a brief description" do
    test "a short description is already enough" do
      description = "Amazing!"
      assert description == Ash.calculate!(Tasks.Task, :brief_description,
                                           refs: %{description: description})
    end

    test "a long description is too much" do
      description = "Amazing article about something interesting!"
      brief = "Amazing article a..."

      assert String.length(brief) == 20
      assert brief == Ash.calculate!(Tasks.Task, :brief_description,
                                     refs: %{description: description})
    end
  end

  describe "Tasks.create_task" do
    test "an user create a new task" do
      user = generate(user())

      expected_title = "title"
      expected_description = "description"

      assert %{
        title: ^expected_title,
        description: ^expected_description
      } = Tasks.create_task!(expected_title, expected_description, actor: user)
    end

    test "a guest can't create a new task" do
      expected_title = "title"
      expected_description = "description"

      assert {
        :error,
        %Ash.Error.Invalid{}
      } = Tasks.create_task(expected_title, expected_description)
    end
  end

  describe "Tasks.read_task" do
    test "an user can read an own task" do
      user = generate(user())
      new_task = generate(task(actor: user))

      assert {:ok, task} = Tasks.get_task(new_task.id, actor: user)
      assert user.id == task.user_id
      assert user.id == task.user_id
      assert task.id == new_task.id
    end

    test "an user can't read an other user's task" do
      user = generate(user())
      task = generate(task())

      assert {:error, %Ash.Error.Query.NotFound{}} = Tasks.get_task(task.id, actor: user)
      refute user.id == task.user_id
    end
  end

  describe "Tasks.list_task" do
    test "an user can read own tasks" do
      user = generate(user())
      task = generate(task(actor: user))

      assert {:ok, [first_task | _]} = Tasks.list_tasks(actor: user)

      assert user.id == task.user_id
      assert user.id == first_task.user_id
      assert task.id == first_task.id
    end

    test "an user can't read other users' tasks" do
      user = generate(user())
      task = generate(task())

      assert {:ok, []} = Tasks.list_tasks(actor: user)
      refute user.id == task.user_id
    end
  end

  describe "Tasks.search_by_title" do
    test "an user can search own tasks" do
      user = generate(user())
      generate_many(task(actor: user), 2)
      limit = 1

      assert {
        :ok,
        page
      } = Tasks.search_by_title("", page: [limit: limit], actor: user)

      assert %{results: results, limit: limit} = page
      assert length(results) == 1

      next_page = Ash.page!(page, :next)
      assert length(next_page.results) == 1
      refute next_page.more?
    end

    test "an user can't search other users' tasks" do
      user = generate(user())
      another_user = generate(user())
      generate_many(task(actor: another_user), 2)
      limit = 1

      assert {
        :ok,
        %{results: [], limit: limit, more?: false}
      } = Tasks.search_by_title("", page: [limit: limit], actor: user)

      # assert } = page
      # refute page.more?
    end
  end

  describe "Tasks.update_task" do
    test "an user can update own tasks" do
      user = generate(user())
      task = generate(task(actor: user))
      new_title =  "new title"
      task = %{task | title: new_title}

      assert {:ok, %{title: new_title}} = Tasks.update_task(task, actor: user)
    end

    test "an user can't update other users' tasks" do
      user = generate(user())
      another_user = generate(user())
      new_title =  "new title"
      task = generate(task(actor: another_user))
      task = %{task | title: new_title}

      refute user.id == task.user_id
      assert {:error, %Ash.Error.Forbidden{}} = Tasks.update_task(task, actor: user)
    end
  end

  describe "Tasks.destroy" do
    test "an user can destroy own tasks" do
      user = generate(user())
      task = generate(task(actor: user))

      assert :ok = Tasks.destroy_task(task, actor: user)
      assert {:error, %Ash.Error.Query.NotFound{}} = Tasks.get_task(task.id)
      assert user.id == task.user_id
    end

    test "an user can't destroy other users' tasks" do
      user = generate(user())
      another_user = generate(user())
      task = generate(task(actor: another_user))

      refute user.id == task.user_id
      assert {:error, %Ash.Error.Forbidden{}} = Tasks.destroy_task(task, actor: user)
    end
  end
end
