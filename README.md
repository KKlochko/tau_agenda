# TauAgenda

**TauAgenda** is a web-server for managing your agenda.

## Getting started
- clone this repository
- copy `.env.example` file and save as `.env`
- copy `.env_db.example` file and save as `.env_db`
- change the values in `.env` and `.env_db`
  you can use `mix phx.gen.secret` to generate secrets
  update the PHX_HOST to your domain name
- if you don't want to pull the pre-built container
  - uncomment the part to build the image in the `docker-compose.yaml` file
  - comment the image part to avoid pulling.
- run `docker compose up -d`
- run migrations `docker exec -it tau_agenda bin/migrate`

# Author

Kostiantyn Klochko (c) 2023-2025

# License

Under the GNU Affero General Public License v3.0.
