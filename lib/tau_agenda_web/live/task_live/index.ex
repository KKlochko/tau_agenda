defmodule TauAgendaWeb.TaskLive.Index do
  use TauAgendaWeb, :live_view

  @impl true
  def render(assigns) do
    ~H"""
    <.header>
      Listing Tasks
      <:actions>
        <.button phx-click="new-task">New Task</.button>
      </:actions>
    </.header>

    <form
      id="task-search-form"
      method="get"
      phx-submit="search"
    >
      <label for="search-title" class="hidden">Search</label>
      <input
        name="title-query"
        id="search-title"
        value={@title_query}
        class="mt-2 mx-2 p-2 block w-full rounded-lg text-zinc-900 focus:ring-0 sm:text-sm sm:leading-6 border-zinc-400 focus:border-zinc-500"
        type="text"
        placeholder="Search by a title"
      >
    </form>

    <.table
      id="tasks"
      rows={@page.results}
      row_click={fn task -> JS.navigate(~p"/tasks/#{task}") end}
    >
      <:col :let={task} label="Title">{task.title}</:col>

      <:col :let={task} label="Description">
        {brief_description(task.description)}
      </:col>

      <:action :let={task}>
        <div class="sr-only">
          <.link navigate={~p"/tasks/#{task}"}>Show</.link>
        </div>

        <.button phx-click="edit-task" phx-value-id={task.id}>Edit</.button>
      </:action>

      <:action :let={task}>
        <.link
          phx-click={JS.push("delete", value: %{id: task.id}) |> hide("##{task.id}")}
          data-confirm="Are you sure?"
        >
          Delete
        </.link>
      </:action>
    </.table>

    <.pagination_control page={@page} title_query={@title_query} />

    <.modal :if={@live_action in [:new, :edit]} id="task-modal" show on_cancel={JS.patch(~p"/tasks")}>
      <.live_component
        module={TauAgendaWeb.TaskLive.FormComponent}
        id={(@task && @task.id) || :new}
        title={@page_title}
        action={@live_action}
        task={@task}
        current_user={@current_user}
      />
    </.modal>
    """
  end

  def pagination_control(assigns) do
    ~H"""
    <div
      :if={
        AshPhoenix.LiveView.prev_page?(@page) ||
        AshPhoenix.LiveView.next_page?(@page)
      }
      class="flex justify-center gap-x-5 pt-8 join"
    >
      <.link patch={~p"/tasks?#{query_params_string(@page, @title_query, "prev")}"} >
        <.button
          data-role="previous-page"
          disabled={!AshPhoenix.LiveView.prev_page?(@page)}
          class="join-item"
          kind="primary"
        >
          Previous
        </.button>
      </.link>

      <.link patch={~p"/tasks?#{query_params_string(@page, @title_query, "next")}"} >
        <.button
          data-role="next-page"
          disabled={!AshPhoenix.LiveView.next_page?(@page)}
          class="join-item"
          kind="primary"
        >
          Next
        </.button>
      </.link>
    </div>
    """
  end

  defp remove_empty(params) do
    Enum.filter(params, fn {_key, val} -> val != "" end)
  end

  def query_params_string(page, title_query, which) do
    case AshPhoenix.LiveView.page_link_params(page, which) do
      :invalid -> []
      list -> list
    end
    |> Keyword.put(:title_query, title_query)
    |> remove_empty()
  end

  @impl true
  def mount(_params, _session, socket) do
    socket =
      socket
      |> assign(:title_query, "")

    {:ok, socket}
  end

  @impl true
  def handle_params(params, _url, socket) do
    title_query = Map.get(params, "title-query", "")
    page_params = AshPhoenix.LiveView.page_from_params(params, 10)

    page = get_current_page(title_query, page_params, socket.assigns.current_user)

    socket =
      socket
      |> assign(:title_query, title_query)
      |> assign(:page, page)
      |> assign(:page_params, page_params)
      |> apply_action(socket.assigns.live_action, params)

    {:noreply, socket}
  end

  defp get_current_page(title_query, page_params, current_user) do
    TauAgenda.Tasks.search_by_title!(
      title_query,
      page: page_params,
      actor: current_user
    )
  end

  @impl true
  def handle_event("edit-task", %{"id" => task_id}, socket) do
    socket =
      socket
      |> assign(:page_title, "Edit Task")
      |> assign(:task, Ash.get!(TauAgenda.Tasks.Task, task_id, actor: socket.assigns.current_user))
      |> assign(:live_action, :edit)

    {:noreply, socket}
  end

  @impl true
  def handle_event("new-task", _params, socket) do
    socket =
      socket
      |> assign(:page_title, "New Task")
      |> assign(:task, nil)
      |> assign(:live_action, :new)

    {:noreply, socket}
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Tasks")
    |> assign(:task, nil)
  end

  @impl true
  def handle_info({TauAgendaWeb.TaskLive.FormComponent, {:saved, task}}, socket) do
    page = get_current_page(
      socket.assigns.title_query,
      socket.assigns.page_params,
      socket.assigns.current_user
    )

    socket =
      socket
      |> assign(:live_action, nil)
      |> assign(:page, page)

    {:noreply, socket}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    task = Ash.get!(TauAgenda.Tasks.Task, id, actor: socket.assigns.current_user)
    Ash.destroy!(task, actor: socket.assigns.current_user)

    page = get_current_page(
      socket.assigns.title_query,
      socket.assigns.page_params,
      socket.assigns.current_user
    )

    socket =
      socket
      |> assign(:page, page)

    {:noreply, socket}
  end

  def handle_event("search", %{"title-query" => query}, socket) do
    {:noreply, push_patch(socket, to: ~p"/tasks?title-query=#{query}")}
  end

  defp brief_description(description) do
    case TauAgenda.Tasks.brief_description(description) do
      {:ok, result} -> result
      {:error, _} -> "Something want wrong"
    end
  end
end
