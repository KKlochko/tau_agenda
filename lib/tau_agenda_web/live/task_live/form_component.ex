defmodule TauAgendaWeb.TaskLive.FormComponent do
  use TauAgendaWeb, :live_component

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        {@title}
        <:subtitle>Use this form to manage tasks.</:subtitle>
      </.header>

      <.simple_form
        for={@form}
        id="task-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input field={@form[:title]} type="text" label="Title" />
        <.input
          field={@form[:status]}
          type="select"
          label="Status"
          options={[
            {"Todo", "todo"},
            {"Doing", "doing"},
            {"Done", "done"},
            {"Canceled", "canceled"}
          ]}
        />
        <.input
          field={@form[:description]}
          type="textarea"
          label="Description"
        />

        <:actions>
          <.button phx-disable-with="Saving...">Save Task</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(assigns, socket) do
    {:ok,
     socket
     |> assign(assigns)
     |> assign_form()}
  end

  @impl true
  def handle_event("validate", %{"task" => task_params}, socket) do
    {:noreply, assign(socket, form: AshPhoenix.Form.validate(socket.assigns.form, task_params))}
  end

  def handle_event("save", %{"task" => task_params}, socket) do
    case AshPhoenix.Form.submit(socket.assigns.form, params: task_params, actor: socket.assigns.current_user) do
      {:ok, task} ->
        notify_parent({:saved, task})

        socket =
          socket
          |> put_flash(:info, "Task #{socket.assigns.form.source.type}d successfully")
          |> push_patch_if()

        {:noreply, socket}

      {:error, form} ->
        {:noreply, assign(socket, form: form)}
    end
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})

  defp assign_form(%{assigns: %{task: task}} = socket) do
    user_id = socket.assigns.current_user.id

    form =
      if task do
        AshPhoenix.Form.for_update(task, :update, as: "task", actor: socket.assigns.current_user)
      else
        AshPhoenix.Form.for_create(
          TauAgenda.Tasks.Task,
          :create,
          as: "task",
          actor: socket.assigns.current_user
        )
      end

    assign(socket, form: to_form(form))
  end

  defp push_patch_if(socket) do
    if Map.has_key?(socket.assigns, :patch) do
      socket
      |> push_patch(to: socket.assigns.patch)
    else
      socket
    end
  end
end
