defmodule TauAgendaWeb.TaskLive.Show do
  use TauAgendaWeb, :live_view

  @impl true
  def render(assigns) do
    ~H"""
    <.header>
      {@task.title}

      <:actions>
        <.link phx-click={JS.push_focus()}>
          <.button phx-click="edit-task" phx-value-id={@task.id}>Edit task</.button>
        </.link>
      </:actions>
    </.header>

    <.list>
      <:item title="Title">{@task.title}</:item>

      <:item title="Status">{@task.status}</:item>

      <:item title="Description">
      </:item>
    </.list>

    <p>{@task.description}</p>

    <.back navigate={~p"/tasks"}>Back to tasks</.back>

    <.modal :if={@live_action == :edit} id="task-modal" show on_cancel={JS.patch(~p"/tasks/#{@task}")}>
      <.live_component
        module={TauAgendaWeb.TaskLive.FormComponent}
        id={@task.id}
        title={@page_title}
        action={@live_action}
        task={@task}
        patch={~p"/tasks/#{@task.id}"}
        current_user={@current_user}
      />
    </.modal>
    """
  end

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:task, Ash.get!(TauAgenda.Tasks.Task, id, actor: socket.assigns.current_user))}
  end

  @impl true
  def handle_event("edit-task", %{"id" => task_id}, socket) do
    socket =
      socket
      |> assign(:task, Ash.get!(TauAgenda.Tasks.Task, task_id, actor: socket.assigns.current_user))
      |> assign(:live_action, :edit)

    {:noreply, socket}
  end

  defp page_title(:show), do: "Show Task"
  defp page_title(:edit), do: "Edit Task"
end
