defmodule TauAgendaWeb.AshJsonApiRouter do
  use AshJsonApi.Router,
    domains: [Module.concat(["TauAgenda.Tasks"])],
    open_api: "/open_api"
end
