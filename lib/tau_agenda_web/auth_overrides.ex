defmodule TauAgendaWeb.AuthOverrides do
  use AshAuthentication.Phoenix.Overrides

  # configure your UI overrides here

  # First argument to `override` is the component name you are overriding.
  # The body contains any number of configurations you wish to override
  # Below are some examples

  # For a complete reference, see https://hexdocs.pm/ash_authentication_phoenix/ui-overrides.html

  override AshAuthentication.Phoenix.Components.Banner do
    # set :image_url, "https://media.giphy.com/media/g7GKcSzwQfugw/giphy.gif"
    set :image_url, nil
    set :dark_image_url, nil
  end

  # override AshAuthentication.Phoenix.Components.SignIn do
  #  set :show_banner false
  # end

  override AshAuthentication.Phoenix.SignInLive do
    set :root_class, "grid h-screen place-items-center dark:bg-gray-900"
  end
end
