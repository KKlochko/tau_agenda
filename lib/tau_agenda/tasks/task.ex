defmodule TauAgenda.Tasks.Task do
  use Ash.Resource,
    otp_app: :tau_agenda,
    domain: TauAgenda.Tasks,
    data_layer: AshPostgres.DataLayer,
    extensions: [AshJsonApi.Resource],
    authorizers: [Ash.Policy.Authorizer]

  postgres do
    table "tasks"
    repo TauAgenda.Repo

    custom_indexes do
      index "title gin_trgm_ops", name: "task_title_gin_index", using: "GIN"
    end
  end

  json_api do
    type "task"
  end

  policies do
    policy action_type(:create) do
      authorize_if actor_present()
    end

    policy action_type(:read) do
      authorize_if relates_to_actor_via(:user)
    end

    policy action_type(:update) do
      authorize_if relates_to_actor_via(:user)
    end

    policy action_type(:destroy) do
      authorize_if relates_to_actor_via(:user)
    end
  end

  actions do
    defaults [:read, :destroy]

    create :create do
      primary? true
      accept [:title, :status, :description]

      change relate_actor(:user)
    end

    update :update do
      primary? true
      accept [:title, :status, :description]
    end

    update :assign_user do
      accept [:user_id]
    end

    update :todo do
      change set_attribute(:status, :todo)
    end

    update :doing do
      change set_attribute(:status, :doing)
    end

    update :done do
      change set_attribute(:status, :done)
    end

    update :cancel do
      change set_attribute(:status, :cancel)
    end

    read :by_id do
      argument :id, :uuid, allow_nil?: false
      get? true
      filter expr(id == ^arg(:id))
    end

    read :search_by_title do
      argument :title, :ci_string do
        constraints allow_empty?: true
        default ""
      end

      filter expr(contains(title, ^arg(:title)))

      pagination offset?: true, default_limit: 10
    end
  end

  attributes do
    uuid_primary_key :id

    attribute :title, :string do
      allow_nil? false
      public? true
    end

    attribute :description, :string do
      public? true
    end

    attribute :status, :task_status do
      allow_nil? false
      default :todo
    end

    timestamps()
  end

  calculations do
    calculate :brief_description, :string, expr(
      if(string_length(description) <= 20) do
        description
      else
        fragment("substr(?, 0, ?)", description, 18) <> "..."
      end
    )
  end

  relationships do
    belongs_to :user, TauAgenda.Accounts.User
  end
end
