defmodule TauAgenda.Tasks.Status do
  use Ash.Type.Enum, values: [:todo, :doing, :done, :canceled]
end
