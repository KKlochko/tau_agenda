defmodule TauAgenda.Tasks do
  use Ash.Domain, otp_app: :tau_agenda, extensions: [AshJsonApi.Domain]

  resources do
    resource TauAgenda.Tasks.Task do
      define :create_task, action: :create, args: [:title, {:optional, :description}]
      define :list_tasks, action: :read
      define :get_task, args: [:id], action: :by_id
      define :update_task, action: :update
      define :destroy_task, action: :destroy
      define :assign_user, args: [:user_id], action: :assign_user
      define :close_task, action: :done
      define :search_by_title, action: :search_by_title, args: [:title]

      define_calculation :brief_description, calculation: :brief_description,
args: [{:ref, :description}]
    end
  end
end
