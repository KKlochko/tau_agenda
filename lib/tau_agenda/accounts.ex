defmodule TauAgenda.Accounts do
  use Ash.Domain,
    otp_app: :tau_agenda

  resources do
    resource TauAgenda.Accounts.Token
    resource TauAgenda.Accounts.User do
      define :get_user_by_id, action: :read, get_by: [:id]
      define :get_user_by_email, action: :get_by_email, args: [:email]
    end
  end
end
